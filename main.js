/**
 * A brief explanation for "project.json":
 * Here is the content of project.json file, this is the global configuration for your game, you can modify it to customize some behavior.
 * The detail of each field is under it.
 {
    "project_type": "javascript",
    // "project_type" indicate the program language of your project, you can ignore this field

    "debugMode"     : 1,
    // "debugMode" possible values :
    //      0 - No message will be printed.
    //      1 - cc.error, cc.assert, cc.warn, cc.log will print in console.
    //      2 - cc.error, cc.assert, cc.warn will print in console.
    //      3 - cc.error, cc.assert will print in console.
    //      4 - cc.error, cc.assert, cc.warn, cc.log will print on canvas, available only on web.
    //      5 - cc.error, cc.assert, cc.warn will print on canvas, available only on web.
    //      6 - cc.error, cc.assert will print on canvas, available only on web.

    "showFPS"       : true,
    // Left bottom corner fps information will show when "showFPS" equals true, otherwise it will be hide.

    "frameRate"     : 60,
    // "frameRate" set the wanted frame rate for your game, but the real fps depends on your game implementation and the running environment.

    "id"            : "gameCanvas",
    // "gameCanvas" sets the id of your canvas element on the web page, it's useful only on web.

    "renderMode"    : 0,
    // "renderMode" sets the renderer type, only useful on web :
    //      0 - Automatically chosen by engine
    //      1 - Forced to use canvas renderer
    //      2 - Forced to use WebGL renderer, but this will be ignored on mobile browsers

    "engineDir"     : "frameworks/cocos2d-html5/",
    // In debug mode, if you use the whole engine to develop your game, you should specify its relative path with "engineDir",
    // but if you are using a single engine file, you can ignore it.

    "modules"       : ["cocos2d"],
    // "modules" defines which modules you will need in your game, it's useful only on web,
    // using this can greatly reduce your game's resource size, and the cocos console tool can package your game with only the modules you set.
    // For details about modules definitions, you can refer to "../../frameworks/cocos2d-html5/modulesConfig.json".

    "jsList"        : [
    ]
    // "jsList" sets the list of js files in your game.
 }
 *
 */
cc.game.onStart = function(){
    //load resources

    //cc.view.adjustViewPort(true);
    //cc.view.setDesignResolutionSize(320, 480, cc.ResolutionPolicy.SHOW_ALL);
    //cc.view.resizeWithBrowserSize(true);

    cc.LoaderScene.preload(["res/HelloWorld.png"], function () {
        var MyScene = cc.Scene.extend({
            onEnter:function () {
                this._super();
                this.addChild(new BackgroundLayer());

                this.initPhysics();
                var size = cc.director.getWinSize();
                var sprite = cc.PhysicsSprite.create("res/stick.png");
                sprite.setName("stick");
                //sprite.setPosition(size.width / 2, 100);
                sprite.setScale(0.4);
                console.log("sprite: "+sprite);
                var contentSize= new Object();
                contentSize.width = 140*0.8;
                contentSize.height = 22*0.8;//sprite.getContentSize();
                console.log("contentSize: "+ contentSize.width+" "+contentSize.height);

                // 2. init the runner physic body
                this.body = new cp.Body(1, cp.momentForBox(1, contentSize.width, contentSize.height));
                //3. set the position of the runner
                this.body.p = cc.p(size.width / 2, 75);
                //4. apply impulse to the body
                //this.body.applyImpulse(cp.v(0, 100), cp.v(10, 0));//run speed
                // body.applyForce(10,5);
                //5. add the created body to space
                this.space.addBody(this.body);
                //6. create the shape for the body
                this.bodyShape = new cp.BoxShape(this.body, contentSize.width, contentSize.height);

                //7. add shape to space
                this.space.addShape(this.bodyShape);
                //8. set body to the physic sprite
                sprite.setBody(this.body);

                this.body.w=3.2;

                this.addChild(sprite, 0);

                //sprite.runAction(new cc.rotateBy(1,100).repeatForever());
                //cc.eventManager.addListener(listener1, this);
                try {

                    cc.eventManager.addListener(cc.EventListener.create({
                        event: cc.EventListener.TOUCH_ONE_BY_ONE,
                        // When "swallow touches" is true, then returning 'true' from the onTouchBegan method will "swallow" the touch event, preventing other listeners from using it.
                        swallowTouches: true,
                        //onTouchBegan event callback function
                        onTouchBegan: touchBegan

                    }), this);


                } catch(err) {

                    //alert("name: " + err.name + "\nmessage: " + err.message + "\nstack: " + err.stack);

                    var label = cc.LabelTTF.create(err.message, "Arial", 12);
                    label.setPosition(size.width / 2, size.height / 2);
                    this.addChild(label, 1);

                }

                /*

                 */
                this.shapes = new Object();

                for(var i=1;i<=7;i++){

                    var ball = cc.PhysicsSprite.create("res/ball.png");;
                    ball.setScale(0.4);
                    ball.setName("ball"+i);
                    var ballContentSize= new Object();
                    ballContentSize.width = 22*0.8;
                    ballContentSize.height = 22*0.8;//sprite.getContentSize();
                    //console.log("contentSize: "+ contentSize.width+" "+contentSize.height);

                    // 2. init the runner physic body
                    var ballBody = new cp.Body(0.01, cp.momentForBox(1, ballContentSize.width, ballContentSize.height));
                    //3. set the position of the runner
                    ballBody.p = cc.p(10+40*i,size.height-100);

                    //ballBody.applyImpulse(cp.v(0, 10), cp.v(0, 0));
                    //5. add the created body to space

                    this.space.addBody(ballBody);
                    //6. create the shape for the body
                    var ballShape = new cp.CircleShape(ballBody, ballContentSize.width/2,cp.vzero);

                    console.log("ball"+i);

                    this.shapes["ball"+i]=ballShape;

                    //7. add shape to space
                    this.space.addShape(ballShape);
                    //8. set body to the physic sprite
                    ball.setBody(ballBody);

                    this.addChild(ball, 0);


                }
                this.ballscount = 7;
                console.log("this.shapes: "+Object.keys(this.shapes).length+" [0]"+ this.shapes["ball1"]);
                /*
                 var label = cc.LabelTTF.create("Hello World", "Arial", 20);
                 label.setPosition(size.width / 2, size.height / 2);
                 this.addChild(label, 1);
                 */
                this.scheduleUpdate();
            },
            initPhysics:function() {
                //1. new space object
                this.space = new cp.Space();
                //2. setup the  Gravity
                this.space.gravity = cp.v(0, 0);

                // 3. set up Walls
                /*

                 var wallBottom = new cp.SegmentShape(this.space.staticBody,
                 cp.v(0, g_groundHight),// start point
                 cp.v(4294967295, g_groundHight),// MAX INT:4294967295
                 0);// thickness of wall
                 this.space.addStaticShape(wallBottom);
                 */
            },update:function (dt) {
                // chipmunk step
                this.space.step(dt);
                //console.log("dt: "+dt);



                var size = cc.director.getWinSize();
                for(var i=1;i<=7;i++) {
                    var ball = this.getChildByName("ball"+i);
                    if(!ball) {
                        continue;
                    }


                    if (ball.body.p.x<0 || ball.body.p.x>size.width || ball.body.p.y<0 || ball.body.p.y>size.height) {

                        console.log("ball: " + i +" is Out");
                        var shape = this.shapes["ball"+i];
                        console.log("ball"+i+" ballShape:"+shape);
                        this.space.removeShape(shape);
                        this.space.removeBody(ball.body);
                        this.removeChild(ball);


                        delete this.shapes["ball"+i];
                        this.ballscount--;

                        if(this.ballscount==0){

                            this.shapes = new Object();;

                            for(var j=1;j<=7;j++){

                                var ball = cc.PhysicsSprite.create("res/ball.png");;
                                ball.setScale(0.4);
                                ball.setName("ball"+j);
                                var ballContentSize= new Object();
                                ballContentSize.width = 22*0.8;
                                ballContentSize.height = 22*0.8;//sprite.getContentSize();
                                //console.log("contentSize: "+ contentSize.width+" "+contentSize.height);

                                // 2. init the runner physic body
                                var ballBody = new cp.Body(0.01, cp.momentForBox(1, ballContentSize.width, ballContentSize.height));
                                //3. set the position of the runner
                                ballBody.p = cc.p(10+40*j,size.height-100);

                                //ballBody.applyImpulse(cp.v(0, 10), cp.v(0, 0));
                                //5. add the created body to space

                                this.space.addBody(ballBody);
                                //6. create the shape for the body
                                var ballShape = new cp.CircleShape(ballBody, ballContentSize.width/2,cp.vzero);
                                console.log("ball"+j);
                                this.shapes["ball"+j]=ballShape;

                                //7. add shape to space
                                this.space.addShape(ballShape);
                                //8. set body to the physic sprite
                                ball.setBody(ballBody);

                                this.addChild(ball, 0);
                            }
                            this.ballscount = 7;



                        }

                    }else {

                        ball.body.vx *= 0.99;
                        ball.body.vy *= 0.99;

                        /*
                         var vel = Math.sqrt(ball.body.vx ^ 2 + ball.body.vy ^ 2);

                         if (vel < 3.0 && vel > 0.0) {
                         console.log("ball: " + i + "body.vel =" + vel);

                         }
                         */
                    }
                }

                var stick = this.getChildByName("stick");
                if (stick.body.p.x<0 || stick.body.p.x>size.width || stick.body.p.y<0 || stick.body.p.y>size.height) {

                    console.log("stick is Out");

                    this.space.removeShape(this.bodyShape);
                    this.space.removeBody(stick.body);
                    this.removeChild(stick);

                    var sprite = cc.PhysicsSprite.create("res/stick.png");
                    sprite.setName("stick");
                    //sprite.setPosition(size.width / 2, 100);
                    sprite.setScale(0.4);
                    // console.log("sprite: "+sprite);
                    var contentSize= new Object();
                    contentSize.width = 140*0.8;
                    contentSize.height = 22*0.8;//sprite.getContentSize();
                    console.log("contentSize: "+ contentSize.width+" "+contentSize.height);

                    // 2. init the runner physic body
                    this.body = new cp.Body(1, cp.momentForBox(1, contentSize.width, contentSize.height));
                    //3. set the position of the runner
                    this.body.p = cc.p(size.width / 2, 75);
                    //4. apply impulse to the body
                    //this.body.applyImpulse(cp.v(0, 100), cp.v(10, 0));//run speed
                    // body.applyForce(10,5);
                    //5. add the created body to space
                    this.space.addBody(this.body);
                    //6. create the shape for the body
                    this.bodyShape = new cp.BoxShape(this.body, contentSize.width, contentSize.height);

                    //7. add shape to space
                    this.space.addShape(this.bodyShape);
                    //8. set body to the physic sprite
                    sprite.setBody(this.body);
                    this.body.w=3.2;

                    this.addChild(sprite, 0);

                }


                //console.log("ball.body.vx = " + ball.body.vx + ", y = " + ball.body.vy);

            }
        });
        cc.director.runScene(new MyScene());
    }, this);

    var listener1 = cc.EventListener.create({
        event: cc.EventListener.MOUSE,
        // When "swallow touches" is true, then returning 'true' from the onTouchBegan method will "swallow" the touch event, preventing other listeners from using it.
        //swallowTouches: true,
        //onTouchBegan event callback function
        onMouseDown: function (event) {
            // event.getCurrentTarget() returns the *listener's* sceneGraphPriority node.
            var target = event.getCurrentTarget();

            var stick = target.getChildByName("stick");

            //Get the position of the current point relative to the button
            var locationInNode = target.convertToNodeSpace(event.getLocation());

            console.log("sprite began... x = " + locationInNode.x + ", y = " + locationInNode.y);
            var size = target.getContentSize();
            var x = locationInNode.x - size.width / 2;
            var y = locationInNode.y;

            console.log("x = " + x + ", y = " + y);

            var angleTan = y/x;

            var vX = 300*Math.cos(Math.atan(angleTan));
            var vY = Math.abs(300*Math.sin(Math.atan(angleTan)));
            if(angleTan<0){

                vX=-vX;
            }
            console.log("vx = " + vX + ", vy = " + vY);

            stick.getBody().applyImpulse(cp.v(vX, vY), cp.v(0, 0));

            var s = target.getContentSize();
            var rect = cc.rect(0, 0, s.width, s.height);

            //Check the click area
            if (cc.rectContainsPoint(rect, locationInNode)) {

                //target.opacity = 180;
                return true;
            }
            return false;
        }

    });
    var listener2 = cc.EventListener.create({
        event: cc.EventListener.TOUCH_ONE_BY_ONE,
        // When "swallow touches" is true, then returning 'true' from the onTouchBegan method will "swallow" the touch event, preventing other listeners from using it.
        swallowTouches: true,
        //onTouchBegan event callback function
        onTouchBegan: touchBegan

    });

    var BackgroundLayer = cc.Layer.extend({
        ctor:function () {
            this._super();
            this.init();
        },

        init:function () {
            this._super();
            var winsize = cc.director.getWinSize();

            //create the background image and position it at the center of screen
            var centerPos = cc.p(winsize.width / 2, winsize.height / 2);
            var spriteBG = new cc.Sprite("res/background.jpg");
            spriteBG.setScale(0.5);
            spriteBG.setPosition(centerPos);
            this.addChild(spriteBG);
        }
    });

    function touchBegan(touch, event) {
        var target = event.getCurrentTarget();

        var stick = target.getChildByName("stick");

        //Get the position of the current point relative to the button
        var locationInNode = target.convertToNodeSpace(touch.getLocation());

        console.log("Touch sprite began... x = " + locationInNode.x + ", y = " + locationInNode.y);
        var size = cc.director.getWinSize();
        var x = locationInNode.x - size.width / 2;
        var y = locationInNode.y-75;
        if(y<0){
            return false;
        }
        console.log("x = " + x + ", y = " + y);

        var angleTan = y/x;

        var vX = 330*Math.cos(Math.atan(angleTan));
        var vY = Math.abs(330*Math.sin(Math.atan(angleTan)));
        if(angleTan<0){

            vX=-vX;
        }
        console.log("vx = " + vX + ", vy = " + vY);

        stick.getBody().applyImpulse(cp.v(vX, vY), cp.v(0, 0));

        return true;
    }

};
cc.game.run("gameCanvas");

	
	


